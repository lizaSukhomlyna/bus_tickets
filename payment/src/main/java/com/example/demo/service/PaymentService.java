package com.example.demo.service;

import com.example.demo.dto.ChangeRequest;
import com.example.demo.model.Payment;

import java.math.BigDecimal;

public interface PaymentService {
    ChangeRequest createPayment(String fio, BigDecimal sum);

    Payment getPaymentById(Long id);

    void  deletePaymentById(Long id);
    void  makeStatusDone(Long id);
}
