package com.example.demo.service.impl;

import com.example.demo.dto.StatusRequest;
import com.example.demo.service.GetStatusService;
import org.springframework.stereotype.Service;

@Service
public class GetStatusImpl implements GetStatusService {
    private static final String[] statuses = new String[]{"DONE", "NEW", "FAILED"};

    @Override
    public StatusRequest getStatus(Long id) {
        int randomWithMathRandom = (int) (Math.random() * 3);
        return new StatusRequest().setStatus(statuses[randomWithMathRandom]);
    }
}
