package com.example.demo.service;

import com.example.demo.dto.StatusRequest;
import org.springframework.stereotype.Service;

@Service
public interface GetStatusService {
    StatusRequest getStatus(Long id);
}
