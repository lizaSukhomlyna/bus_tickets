package com.example.demo.service.impl;


import com.example.demo.dto.ChangeRequest;
import com.example.demo.model.Payment;
import com.example.demo.repository.PaymentRepository;
import com.example.demo.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public ChangeRequest createPayment(String fio, BigDecimal sum) {
        Payment p = new Payment();
        p.setFio(fio);
        p.setSum(sum);
        p.setStatusPayment("NEW");
        return new ChangeRequest().setId(paymentRepository.save(p).getId());
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Payment wasn't found"));
    }

    @Override
    public void deletePaymentById(Long id) {
        paymentRepository.deleteById(id);
    }

    @Override
    public void makeStatusDone(Long id) {
            Payment payment = paymentRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Payment wasn't found"));
            payment.setStatusPayment("DONE");
            paymentRepository.save(payment);
    }
}
