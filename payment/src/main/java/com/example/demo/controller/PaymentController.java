package com.example.demo.controller;


import com.example.demo.dto.ChangeRequest;
import com.example.demo.dto.StatusRequest;
import com.example.demo.model.Payment;
import com.example.demo.service.PaymentService;
import com.example.demo.service.GetStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/payment")
public class PaymentController {


    @Autowired
    PaymentService paymentService;

    @Autowired
    GetStatusService getStatusService;

    @PostMapping("/create")
    public ChangeRequest createPayment(@RequestBody Payment payment) {
        return paymentService.createPayment(payment.getFio(), payment.getSum());
    }

    @GetMapping("/getPayment")
    public Payment getPayment(@RequestParam Long id) {
        return paymentService.getPaymentById(id);
    }

    @GetMapping("/status")
    public StatusRequest getStatus(@RequestParam Long id) {
        return getStatusService.getStatus(id);
    }

    @PostMapping("/delete")
    public void deletePayment(@RequestBody ChangeRequest req) {

        paymentService.deletePaymentById(req.getId());
    }

    @PostMapping("/makeStatusDone")
    public void makeStatusDone(@RequestBody ChangeRequest req) {
        paymentService.makeStatusDone(req.getId());
    }


}
