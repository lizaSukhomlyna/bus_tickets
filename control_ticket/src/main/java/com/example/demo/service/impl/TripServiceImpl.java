package com.example.demo.service.impl;

import com.example.demo.dto.TripChangeResponse;
import com.example.demo.model.Trip;
import com.example.demo.repository.TripRepository;
import com.example.demo.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TripServiceImpl implements TripService {

    @Autowired
    TripRepository tripRepository;
    @Override
    public TripChangeResponse createTrip(Trip trip) {
        return new TripChangeResponse().setId(tripRepository.save(trip).getId());
    }
}
