package com.example.demo.service.impl;


import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentChangeResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import com.example.demo.service.BuyTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class BuyTicketServiceImpl implements BuyTicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    TripRepository tripRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PaymentsDao paymentsDao;


    @Override
    public PaymentChangeResponse buyTicket(String fio, Long idTrip) {
        Ticket ticket = new Ticket();
        Trip trip = tripRepository.findById(idTrip)
                .orElseThrow(() -> new RuntimeException("Trip wasn't found"));
        trip.setCountTickets(trip.getCountTickets() - 1);
        ticket.setFio(fio);
        ticket.setIdTrip(idTrip);
        ticket.setIdPayment(paymentsDao.createPayment(fio, trip.getCost()).getId());

        return new PaymentChangeResponse().setId(ticketRepository.save(ticket).getId());
    }
}
