package com.example.demo.service;

import com.example.demo.dto.TicketInfoResponse;

public interface TicketInfoService {


    TicketInfoResponse getInfo(Long idTicket);
}
