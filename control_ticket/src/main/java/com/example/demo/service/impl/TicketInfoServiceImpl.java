package com.example.demo.service.impl;


import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentResponse;
import com.example.demo.dto.TicketInfoResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import com.example.demo.service.TicketInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TicketInfoServiceImpl implements TicketInfoService {


    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    TripRepository tripRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PaymentsDao paymentsDao;

    @Override
    public TicketInfoResponse getInfo(Long idTicket) {
        TicketInfoResponse ticketInfoResponse = new TicketInfoResponse();
        Ticket ticket = ticketRepository
                .findById(idTicket)
                .orElseThrow(() -> new RuntimeException("Ticket wasn't found"));
        Trip trip = tripRepository.findById(ticket.getIdTrip())
                .orElseThrow(() -> new RuntimeException("Trip wasn't found"));

        PaymentResponse payment = paymentsDao.getPayment(ticket.getIdPayment());
        ticketInfoResponse
                .setDestination(trip.getDestination())
                .setCountTickets(trip.getCountTickets())
                .setTime(trip.getTime())
                .setDeparture(trip.getDeparture())
                .setCost(trip.getCost())
                .setStatus(payment.getStatusPayment());

        return ticketInfoResponse;
    }
}
