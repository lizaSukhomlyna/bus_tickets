package com.example.demo.service;

import com.example.demo.dto.TripChangeResponse;
import com.example.demo.model.Trip;

public interface TripService {
    TripChangeResponse createTrip(Trip trip);
}
