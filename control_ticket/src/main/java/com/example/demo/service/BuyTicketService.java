package com.example.demo.service;

import com.example.demo.dto.PaymentChangeResponse;

public interface BuyTicketService {
    PaymentChangeResponse buyTicket(String fio, Long idTrip);

}
