package com.example.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class PaymentCreateRequest {
    private BigDecimal sum;
    private String fio;
}
