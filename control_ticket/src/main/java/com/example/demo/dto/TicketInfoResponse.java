package com.example.demo.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
@ToString
@Accessors(chain = true)
public class TicketInfoResponse {
    private String departure;
    private String destination;
    private LocalDateTime time;
    private BigDecimal cost;
    private Integer countTickets;
    private String status;
}
