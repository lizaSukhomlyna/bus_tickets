package com.example.demo.controller;


import com.example.demo.dto.PaymentChangeResponse;
import com.example.demo.dto.TicketInfoResponse;
import com.example.demo.dto.TripChangeResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.service.BuyTicketService;
import com.example.demo.service.TicketInfoService;
import com.example.demo.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/controlTicket")
public class ControlTicketController {


    @Autowired
    BuyTicketService buyTicketService;

    @Autowired
    TicketInfoService ticketInfoService;

    @Autowired
    TripService tripService;

    @PostMapping("/buy")
    public PaymentChangeResponse createPayment(@RequestBody Ticket ticket) {
        return buyTicketService.buyTicket(ticket.getFio(), ticket.getIdTrip());
    }

    @PostMapping("/createTrip")
    public TripChangeResponse createTrip(@RequestBody Trip trip) {

        return tripService.createTrip(trip);
    }

    @GetMapping("/ticketInfo")
    public TicketInfoResponse getPayment(@RequestParam Long id) {
        return ticketInfoService.getInfo(id);
    }

}
