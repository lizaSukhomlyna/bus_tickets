package com.example.demo.model;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tickets")
@EqualsAndHashCode(of = "ticket_id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Ticket {

    @Id
    @Column(name = "ticket_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String fio;
    private Long idTrip;
    private Long idPayment;


}
