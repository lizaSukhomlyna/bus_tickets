package com.example.demo.model;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "trips")
@EqualsAndHashCode(of = "trip_id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Trip {

    @Id
    @Column(name = "trip_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String departure;
    private String destination;
    private LocalDateTime time;
    private BigDecimal cost;
    private Integer countTickets;
}
