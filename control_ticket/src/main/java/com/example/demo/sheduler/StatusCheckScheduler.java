package com.example.demo.sheduler;
import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.stream.StreamSupport;


@Component
public class StatusCheckScheduler {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    TripRepository tripRepository;


    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PaymentsDao paymentsDao;


    @Scheduled(fixedRateString = "${shedule.fixedRate}")
    public void reportCurrentData() {
        List<Ticket> tickets = StreamSupport
                .stream(ticketRepository.findAll().spliterator(), false).toList();
        if (!tickets.isEmpty()) {
            List<Long> idPayments = tickets.stream()
                    .map(Ticket::getIdPayment).toList();
            idPayments.forEach(id -> {
                PaymentResponse payment = paymentsDao.getPayment(id);
                if (payment != null && payment.getStatusPayment().equals("NEW")) {
                    String getStatus = paymentsDao.getPaymentStatus(id);
                    if (getStatus == null) {
                        return;
                    }
                    switch (getStatus) {
                        case "FAILED":
                            paymentsDao.deletePayment(id);
                            Ticket ticket = updateTicketInfo(tickets, id);
                            updateTripInfo(ticket);
                            break;
                        case "DONE":
                            paymentsDao.makeStatusDone(id);
                            break;

                    }
                    payment.setStatusPayment(getStatus);

                }
            });
        }

    }

    private void updateTripInfo(Ticket ticket) {
        Trip trip = tripRepository.findById(ticket.getIdTrip())
                .orElseThrow(() -> new RuntimeException("Trip wasn't found"));
        trip.setCountTickets(trip.getCountTickets() - 1);
    }

    private Ticket updateTicketInfo(List<Ticket> tickets, Long id) {
        Ticket ticket = tickets.stream()
                .filter(el -> Objects.equals(el.getIdPayment(), id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Ticket wasn't found"));
        ticketRepository.deleteById(ticket.getId());
        return ticket;
    }
}
