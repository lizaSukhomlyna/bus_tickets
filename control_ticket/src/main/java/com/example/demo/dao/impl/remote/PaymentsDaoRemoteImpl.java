package com.example.demo.dao.impl.remote;

import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentChangeResponse;
import com.example.demo.dto.PaymentCreateRequest;
import com.example.demo.dto.PaymentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Component
public class PaymentsDaoRemoteImpl implements PaymentsDao {
    @Autowired
    RestTemplate restTemplate;

    @Value("${url.create.payment}")
    private String urlCreate;

    @Value("${url.get.payment}")
    private String urlGet;
    @Value("${url.get.payment.status}")
    private String urlGetStatus;
    @Value("${url.delete.payment}")
    private String urlDeletePament;
    @Value("${url.makeStatus.done.payment}")
    private  String urlMakeStatusDone;

    @Override
    public PaymentChangeResponse createPayment(String fio, BigDecimal sum) {
        return restTemplate.postForObject(urlCreate,
                new PaymentCreateRequest().setFio(fio).setSum(sum),
                PaymentChangeResponse.class
        );
    }

    @Override
    public PaymentResponse getPayment(Long id) {
       return restTemplate.getForObject(urlGet + id, PaymentResponse.class);
    }

    @Override
    public String getPaymentStatus(Long id) {
        return restTemplate.getForObject(urlGetStatus + id, String.class);
    }

    @Override
    public void deletePayment(Long id) {
        restTemplate.postForObject(urlDeletePament,
                new PaymentChangeResponse().setId(id),
                PaymentChangeResponse.class
        );
    }

    @Override
    public void makeStatusDone(Long id) {
        restTemplate.postForObject(urlMakeStatusDone,
                new PaymentChangeResponse().setId(id),
                PaymentChangeResponse.class
        );
    }

}
