package com.example.demo.dao;

import com.example.demo.dto.PaymentChangeResponse;
import com.example.demo.dto.PaymentResponse;

import java.math.BigDecimal;

public interface PaymentsDao {

    PaymentChangeResponse createPayment(String fio, BigDecimal sum);

    PaymentResponse getPayment(Long id);

    String getPaymentStatus(Long id);

    void deletePayment(Long id);

    void makeStatusDone(Long id);


}
