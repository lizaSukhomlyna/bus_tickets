package com.example.demo.service.impl;

import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentChangeResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;

class BuyTicketServiceImplTest {

    @Mock
    TicketRepository ticketRepository;

    @Mock
    TripRepository tripRepository;

    @Mock
    PaymentsDao paymentsDao;

    @InjectMocks
    BuyTicketServiceImpl buyTicketService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        Trip trip = new Trip(1L, "Departure", "Destination", LocalDateTime.of(2023, 10, 2, 19, 10), BigDecimal.TEN, 100);
        when(tripRepository.findById(1L)).thenReturn(Optional.of(trip));
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setFio("Fio");
        ticket.setIdPayment(1L);
        ticket.setIdTrip(1L);
        when(ticketRepository.save(ticket)).thenReturn(ticket);
    }

    @Test
    void buyTicket() {
        Assertions.assertEquals(new PaymentChangeResponse().setId(1L),
                buyTicketService.buyTicket("Fio", 1L));

        Assertions.assertThrows(RuntimeException.class,
                () -> buyTicketService.buyTicket("Fio2", 2L));
    }
}