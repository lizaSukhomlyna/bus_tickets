package com.example.demo.service.impl;

import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentResponse;
import com.example.demo.dto.TicketInfoResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;

class TicketInfoServiceImplTest {

    @Mock
    TicketRepository ticketRepository;

    @Mock
    TripRepository tripRepository;

    @InjectMocks
    TicketInfoServiceImpl ticketInfoService;

    @Mock
    PaymentsDao paymentsDao;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        Trip trip = new Trip(1L, "Departure", "Destination",
                LocalDateTime.of(2023, 10, 2, 19, 10),
                BigDecimal.TEN, 100);
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setFio("Fio");
        ticket.setIdPayment(1L);
        ticket.setIdTrip(1L);
        when(ticketRepository.findById(1L)).thenReturn(Optional.of(ticket));
        when(tripRepository.findById(1L)).thenReturn(Optional.of(trip));
        PaymentResponse paymentResponse = new PaymentResponse()
                .setStatusPayment("NEW").setId(1L).setFio("Fio").setSum(BigDecimal.TEN);
        when(paymentsDao.getPayment(1L)).thenReturn(paymentResponse);
    }

    @Test
    void getInfo() {
        TicketInfoResponse ticketInfoResponse = new TicketInfoResponse();
        ticketInfoResponse.setCountTickets(100);
        ticketInfoResponse.setDestination("Destination");
        ticketInfoResponse.setTime(LocalDateTime.of(2023, 10, 2, 19, 10));
        ticketInfoResponse.setDeparture("Departure");
        ticketInfoResponse.setStatus("NEW");
        ticketInfoResponse.setCost(BigDecimal.TEN);
        Assertions.assertEquals(ticketInfoResponse, ticketInfoService.getInfo(1L));
        Assertions.assertThrows(RuntimeException.class, () -> ticketInfoService.getInfo(2L));

    }
}