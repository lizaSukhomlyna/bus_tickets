package com.example.demo.sheduler;

import com.example.demo.dao.PaymentsDao;
import com.example.demo.dto.PaymentResponse;
import com.example.demo.model.Ticket;
import com.example.demo.model.Trip;
import com.example.demo.repository.TicketRepository;
import com.example.demo.repository.TripRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class StatusCheckSchedulerTest {

    @Mock
    TicketRepository ticketRepository;
    @Mock
    TripRepository tripRepository;


    @Mock
    PaymentsDao paymentsDao;

    @InjectMocks
    StatusCheckScheduler statusCheckScheduler;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        Trip trip = new Trip(1L, "Departure", "Destination", LocalDateTime.of(2023, 10, 2, 19, 10), BigDecimal.TEN, 100);
        when(tripRepository.findById(1L)).thenReturn(Optional.of(trip));
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(new Ticket(1L, "Fio1", 1L, 1L));
        tickets.add(new Ticket(2L, "Fio2", 1L, 2L));

        PaymentResponse firstPayment = new PaymentResponse(1L, "Fio1", BigDecimal.TEN, "NEW");
        PaymentResponse secondPayment = new PaymentResponse(2L, "Fio2", BigDecimal.TEN, "NEW");

        when(ticketRepository.findAll()).thenReturn(tickets);
        when(paymentsDao.getPayment(1L)).thenReturn(firstPayment);
        when(paymentsDao.getPayment(2L)).thenReturn(secondPayment);
        when(paymentsDao.getPaymentStatus(1L)).thenReturn("FAILED");
        when(paymentsDao.getPaymentStatus(2L)).thenReturn("DONE");

    }

    @Test
    void reportCurrentData() {
        statusCheckScheduler.reportCurrentData();
        verify(paymentsDao).deletePayment(1L);
        verify(paymentsDao).makeStatusDone(2L);

    }
}